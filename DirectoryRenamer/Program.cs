﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DirectoryRenamer
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = GetConfig();
            var result = PerformRename(config);
            ShowResults(result);
        }

        private static RenameConfig GetConfig()
        {
            Console.WriteLine("Welcome to the Directory Renamer");

            var config = new RenameConfig();
            SetDirectory(config);

            config.ReplacementToken = GetValue("Token to replace? ");
            config.ReplacementValue = GetValue(string.Format("Value to replace {0} with? ", config.ReplacementToken));
            config.ReplaceInFileContents = GetYesNo("Replace token in file contents? (y/n) ");
            return config;
        }

        private static RenameResult PerformRename(RenameConfig config)
        {
            Console.WriteLine("Working....");
            var renamer = new Renamer(config);
            var result = renamer.Execute();
            return result;
        }

        private static void ShowResults(RenameResult result)
        {
            Console.WriteLine("Done!");
            Console.WriteLine("{0} directories renamed", result.DirectoriesRenamed);
            Console.WriteLine("{0} files renamed", result.FilesRenamed);
            Console.WriteLine("{0} content matches renamed", result.ContentsRenamed);
        }

        private static void SetDirectory(RenameConfig config)
        {
            while (!Directory.Exists(config.Directory))
            {
                config.Directory = GetValue("Directory? ");
            }
        }

        private static bool GetYesNo(string prompt)
        {
            string value = string.Empty;
            while (!value.Equals("y") && !value.Equals("n"))
            {
                value = GetValue(prompt);
            }
            return value.Equals("y");
        }

        private static string GetValue(string prompt)
        {
            Console.Write(prompt);
            return Console.ReadLine();
        }
    }
}
