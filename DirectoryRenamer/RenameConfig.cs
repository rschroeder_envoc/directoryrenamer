namespace DirectoryRenamer
{
    public class RenameConfig
    {
        public RenameConfig()
        {
            ReplaceInFileContents = true;
        }

        public string Directory { get; set; }

        public string ReplacementToken { get; set; }

        public string ReplacementValue { get; set; }

        public bool ReplaceInFileContents { get; set; }

        public string[] DirectoriesToIgnore
        {
            get { return new[] { ".svn", ".git" }; }
        }

        public string[] FilesToIgnore
        {
            get
            {
                return new[] {
                    "exe","dll","pdb","jpg","png","gif","mst","msi","msm",
                    "gitignore","idx","pack","suo","ico","nupkg","zip","7z"
                };
            }
        }
    }
}