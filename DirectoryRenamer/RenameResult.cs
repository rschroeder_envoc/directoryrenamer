namespace DirectoryRenamer
{
    public class RenameResult
    {
        public int DirectoriesRenamed { get; set; }

        public int FilesRenamed { get; set; }

        public int ContentsRenamed { get; set; }
    }
}