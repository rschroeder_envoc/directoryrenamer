using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Ionic.Zip;

namespace DirectoryRenamer
{
    public class Renamer
    {
        private readonly RenameConfig config;
        private RenameResult result;

        public Renamer(RenameConfig config)
        {
            this.config = config;
        }

        public RenameResult Execute()
        {
            result = new RenameResult();
            BackupDirectory();
            var rootDirectory = new DirectoryInfo(config.Directory);

            ReplaceTokensInFolder(rootDirectory);
            ReplaceTokensInFiles(rootDirectory);
            if (config.ReplaceInFileContents)
            {
                ReplaceTokensInFileContents(rootDirectory);
            }

            return result;
        }

        private void BackupDirectory()
        {
            string zipFilePath = Path.Combine(config.Directory, string.Format("RenameBackup-{0:MMddyyyyhhmmss}.zip", DateTime.Now));
            using (var zip = new ZipFile())
            {
                zip.AddDirectory(config.Directory);
                zip.Save(zipFilePath);
            }
        }

        private void ReplaceTokensInFolder(DirectoryInfo directory)
        {
            if (config.DirectoriesToIgnore.Contains(directory.Name)) return;

            foreach (var childDirectory in directory.GetDirectories())
            {
                ReplaceTokensInFolder(childDirectory);
            }

            if (config.Directory.Equals(directory.FullName)) return;

            if (directory.Name.Contains(config.ReplacementToken))
            {
                string newFolderName = directory.Name.Replace(config.ReplacementToken, config.ReplacementValue);
                string newPath = Path.Combine(directory.Parent.FullName, newFolderName);
                directory.MoveTo(newPath);
                result.DirectoriesRenamed++;
            }
        }

        private void ReplaceTokensInFiles(DirectoryInfo directory)
        {
            if (config.DirectoriesToIgnore.Contains(directory.Name)) return;

            foreach (var childDirectory in directory.GetDirectories())
            {
                ReplaceTokensInFiles(childDirectory);
            }

            var files = directory.GetFiles("*.*", SearchOption.TopDirectoryOnly)
                .Where(x => x.Name.Contains(config.ReplacementToken));
            foreach (var file in files)
            {
                string relativePath = file.FullName.Replace(config.Directory, string.Empty);
                string newFileName = relativePath.Replace(config.ReplacementToken, config.ReplacementValue);
                string newFullPath = Path.Combine(config.Directory, newFileName);
                file.MoveTo(newFullPath);
                result.FilesRenamed++;
            }
        }

        private void ReplaceTokensInFileContents(DirectoryInfo directory)
        {
            if (config.DirectoriesToIgnore.Contains(directory.Name)) return;

            foreach (var childDirectory in directory.GetDirectories())
            {
                ReplaceTokensInFileContents(childDirectory);
            }

            foreach (var info in directory.GetFiles("*.*", SearchOption.TopDirectoryOnly))
            {
                if (config.FilesToIgnore.Contains(info.Extension)) continue;

                string contents = File.ReadAllText(info.FullName);
                result.ContentsRenamed += new Regex(config.ReplacementToken).Matches(contents).Count;
                contents = contents.Replace(config.ReplacementToken, config.ReplacementValue);
                File.WriteAllText(info.FullName, contents);
            }
        }
    }
}